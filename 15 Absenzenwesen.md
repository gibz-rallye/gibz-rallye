# 15 - Absenzenwesen

## Standort

- UUID: `221a4c7a-4ce6-4bea-a3ed-957c10e15780`
- Major: `116`

## Instruktion

Gehen Sie zum Aufenthaltsbereich beim Haupteingang des Trakts 5.

## Information

Im [Artikel 21 des Bundesgesetz über die Berufsbildung](https://www.fedlex.admin.ch/eli/cc/2003/674/de#art_21) (Absatz 3) ist geregelt: *Der Besuch der Berufsfachschule ist obligatorisch*.

Dennoch kann es während Ihrer Ausbildung am GIBZ zu Absenzen kommen.

Im Video sind das Verhalten bei Absenzen und die Handhabung dieser Absenzen detailliert erklärt. Schauen Sie sich das Video aufmerksam an.

## Anhänge

- Video: [https://storage.googleapis.com/ch_gibz_app_video/2023/15_Absenzen.mp4](https://storage.googleapis.com/ch_gibz_app_video/2023/15_Absenzen.mp4)

## Challenges

### Challenge 16-01 [SC]
Innerhalb welcher Frist müssen Absenzen entschuldigt werden?

- [ ] 2 Wochen
- [x] 4 Wochen
- [ ] 10 Arbeitstage
- [ ] 15 Arbeitstage

### Challenge 16-02 [SC]
Wer wird per E-Mail über die Absenz von Lernenden informiert?

- [x] Der Arbeitgeber
- [ ] Die Eltern
- [ ] Die Lehrpersonen
- [ ] Das Sekretariat

### Challenge 16-03 [MC]
Wo sind die persönlichen, nicht entschuldigten Absenzen aufgeführt?

*Wählen Sie eine oder mehrere Antwortmöglichkeiten aus. Mindestens eine Antwortmöglichkeit ist richtig.*

- [ ] Auf dem Info-Screen
- [x] Online im schulNetz
- [ ] Auf Teams
- [x] Im Zeugnis
- [ ] Im monatlichen Newsletter

### Challenge 16-04 [MC]
In welchem Fall unterschreiben die Eltern eine Absenz, damit diese als *entschuldigt* gilt?

*Wählen Sie eine oder mehrere Antwortmöglichkeiten aus. Mindestens eine Antwortmöglichkeit ist richtig.*

- [x] Bei Lernenden unter 18 Jahren
- [ ] Bei Lernenden über 18 Jahren
- [ ] Bei lange dauernden Absenzen
- [ ] Wenn der Lehrbetrieb nicht unterschreibt