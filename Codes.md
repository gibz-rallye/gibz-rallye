# Codes für die GIBZ Rallye

Die nachfolgenden, sechsstelligen Codes können von den Lernenden der jeweiligen Klasse zum Starten der GIBZ Rallye verwendet werden. Das Leerzeichen nach den ersten drei Ziffern ist für die Eingabe in der GIBZ App irrelevant und dient lediglich der besseren Lesbarkeit.

Zum **Testen** der Rallye können Lehrpersonen und Mitarbeitende des GIBZ den Code `232323` verwenden.

| Klasse | Rallye-Code |     | Klasse  | Rallye-Code |     | Klasse | Rallye-Code |
| ------ | :---------: | --- | ------- | :---------: | --- | ------ | :---------: |
| ABUAT1 |  `720 635`  |     | FABE1a  |  `334 478`  |     | INFP1a |  `434 954`  |
| ABUE1a |  `183 642`  |     | FABE1b  |  `313 797`  |     | INFP1b |  `218 940`  |
| ABUE1b |  `389 522`  |     | FABE1c  |  `938 367`  |     | INVOL1 |  `234 335`  |
| AF1    |  `460 300`  |     | FABE1d  |  `179 683`  |     | KO1a   |  `208 809`  |
| AMT1   |  `284 553`  |     | FABE1e  |  `869 657`  |     | KO1b   |  `292 487`  |
| ATAA1  |  `979 615`  |     | FAGE1a  |  `332 402`  |     | KO1c   |  `688 836`  |
| ATAGS1 |  `274 020`  |     | FAGE1b  |  `411 030`  |     | KR1    |  `222 159`  |
| ATEP1  |  `192 555`  |     | FAGE1c  |  `290 369`  |     | MOE1a  |  `210 418`  |
| ATHW1  |  `538 279`  |     | FAGEEB1 |  `219 202`  |     | MOE1b  |  `126 190`  |
| ATKU1  |  `895 122`  |     | FAGEV1a |  `578 847`  |     | MR1    |  `104 670`  |
| ATRP1  |  `273 301`  |     | FAGEV1b |  `387 303`  |     | PM1    |  `931 152`  |
| AU1    |  `136 895`  |     | FAGEV1c |  `782 447`  |     | SI1    |  `874 809`  |
| CO1    |  `514 444`  |     | FAGEV1d |  `499 330`  |     | SR1    |  `334 423`  |
| EI1a   |  `704 783`  |     | FHW1    |  `483 255`  |     | ZA1a   |  `286 810`  |
| EI1b   |  `832 286`  |     | ICT1    |  `977 369`  |     | ZA1b   |  `174 581`  |
| ELO1   |  `362 217`  |     | INFA1a  |  `380 959`  |     | ZFA1a  |  `550 526`  |
| ET1a   |  `502 071`  |     | INFA1b  |  `165 073`  |     | ZFA1b  |  `739 114`  |
| ET1b   |  `372 304`  |     | INFA1f  |  `734 472`  |     | ZFI1   |  `930 780`  |
|        |             |     | INFAWU1 |  `434 954`  |     |        |             |
