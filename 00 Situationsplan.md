# 00 - Situationsplan

## Standort

- UUID: `221a4c7a-4ce6-4bea-a3ed-957c10e15780`
- Major: `100`

## Instruktion
Gehen Sie **im Erdgeschoss** zu einer Informationstafel, auf welcher der **Situationsplan des GIBZ** abgebildet ist.

Eine solche Informationstafel ist grundsätzlich in jedem Trakt, auf jedem Stockwerk zu finden. Für den Start dieser Rallye müssen Sie jedoch zu einer Informationstafel *im Erdgeschoss* gehen.

## Information

Der Situationsplan zeigt auf der linken Seite eine **Darstellung der fünf Gebäudeteile**, aus welchen das GIBZ besteht. Auf der rechten Seite sind **trakt- und stockwerkspezifische** Informationen aufgeführt.

Das Video informiert Sie über die Systematik, nach welcher die **Zimmernummern am GIBZ** aufgebaut sind. Schauen Sie sich dieses Video aufmerksam an.

## Anhänge

- Video: [https://storage.googleapis.com/ch_gibz_app_video/2023/00_Situationsplan.mp4](https://storage.googleapis.com/ch_gibz_app_video/2023/00_Situationsplan.mp4)

## Challenges

### Challenge 00-01 [SC]
Auf welchem Stockwerk befindet sich das Zimmer `2.303`?

- [ ] 1. Stock
- [ ] 2. Stock
- [x] 3. Stock
- [ ] 4. Stock

### Challenge 00-02 [SC]
Auf welchem Stockwerk befindet sich das Zimmer `2.401`?

- [ ] 1. Stock
- [ ] 2. Stock
- [ ] 3. Stock
- [x] 4. Stock

### Challenge 00-03 [SC]
Auf welchem Stockwerk befindet sich das Zimmer `3.218`?

- [ ] 1. Stock
- [x] 2. Stock
- [ ] 3. Stock
- [ ] 4. Stock

### Challenge 00-04 [SC]
In welchem Trakt befindet sich das Zimmer `3.218`?

- [ ] Trakt 1
- [ ] Trakt 2
- [x] Trakt 3
- [ ] Trakt 4
- [ ] Trakt 5

### Challenge 00-05 [SC]
In welchem Trakt befindet sich das Zimmer `5.211`?

- [ ] Trakt 1
- [ ] Trakt 2
- [ ] Trakt 3
- [ ] Trakt 4
- [x] Trakt 5