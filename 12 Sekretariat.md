# 12 - Sekretariat

## Standort

- UUID: `221a4c7a-4ce6-4bea-a3ed-957c10e15780`
- Major: `113`

## Instruktion

Gehen Sie zum Sekretariat des GIBZ.

Das Sekretariat befindet sich im Trakt 1 auf dem 1. Stockwerk, direkt neben dem Restaurant *Treff*.

## Information

Im Sekretariat des GIBZ laufen alle administrativen und organisatorischen Prozesse rund um den Schulbetrieb zusammen. Angefangen bei der Erfassung aller Lernenden nach der Unterzeichnung des Lehrvertrages bis hin zur Meldung der Schulnoten für die Erstellung der Abschlusszeugnisse.

Im Video erfahren Sie viel Wissenswertes über die Aufgaben und Dienstleistungen des Sekretariats.

## Anhänge

- Video: [https://storage.googleapis.com/ch_gibz_app_video/2023/12_Sekretariat.mp4](https://storage.googleapis.com/ch_gibz_app_video/2023/12_Sekretariat.mp4)

## Challenges

### Challenge 13-01 [SC]
Wann ist der Schalter des Sekretariats während der Schulzeit geöffnet?

- [x] 07:30 - 14:00 Uhr
- [ ] 07:00 - 12:00 Uhr
- [ ] 08:00 - 12:00 Uhr und 13:00 - 17:00 Uhr
- [ ] 08:00 - 17:00 Uhr

### Challenge 13-02 [MC]
Was sind Dienstleistungen des Sekretariats?

*Wählen Sie eine oder mehrere Antwortmöglichkeiten aus. Mindestens eine Antwortmöglichkeit ist richtig.*

- [x] Saldo-Karten für das Parkhaus ausstellen
- [ ] Herausgabe des Lift-Schlüssels
- [ ] Ausgabe von Fundgegenständen
- [x] Verarbeiten der online eingereichten Adressänderungen

### Challenge 13-03 [SC]
Wie lautet die [Telefonnummer des Sekretariats](https://www.gibz.ch)?

- [ ] 041 728 30 00
- [x] 041 728 30 30
- [ ] 041 728 30 60
- [ ] 145

### Challenge 13-04 [MC]
Wie sind die Mitarbeiterinnen des Sekretariats während den Schulferien erreichbar?

*Wählen Sie eine oder mehrere Antwortmöglichkeiten aus. Mindestens eine Antwortmöglichkeit ist richtig.*

- [ ] via Teams
- [x] per E-Mail
- [ ] am Schalter
- [x] telefonisch von 8 bis 11 Uhr