# 02 - Sportunterricht

## Standort

- UUID: `221a4c7a-4ce6-4bea-a3ed-957c10e15780`
- Major: `103`

## Instruktion

Gehen Sie zum Eingang der Sporthalle 1.

Die Sporthallen befinden sich im Untergeschoss des Trakts 2.

## Information

Das GIBZ verfügt über eine umfangreiche und vielfältige Infrastruktur für den Sportunterricht. In der Sporthalle 1 können Sie eine Kletterwand erkennen.

Weitere Anlagen und interessante Informationen zum Sportunterricht am GIBZ sehen Sie im Video.

*Falls Sie das Video wegen schlechtem Empfang nicht anschauen können: Gehen Sie die Treppe hoch spielen Sie das Video im Erdgeschoss ab.*

## Anhänge

- Video: [https://storage.googleapis.com/ch_gibz_app_video/2023/02_Sportunterricht.mp4](https://storage.googleapis.com/ch_gibz_app_video/2023/02_Sportunterricht.mp4)
- Link: [Sportheft](https://www.zg.ch/behoerden/volkswirtschaftsdirektion/gibz/grundbildung/bm-abu-sport/sport-sp/downloads/sportheft/download)
- Link: [Bewertungskriterien für den Sportunterricht](https://www.zg.ch/behoerden/volkswirtschaftsdirektion/gibz/grundbildung/bm-abu-sport/sport-sp/downloads/bewertungskriterien-im-sportunterricht-am-gibz/download)

## Challenges

### Challenge 03-01 [MC]
Welche Disziplinen sind Bestandteil des **GIBZ-Fitnesstests**?

Tipp: Für die Beantwortung dieser Frage könnte ein Dokument im Informationsteil dieser Station hilfreich sein.

*Wählen Sie eine oder mehrere Antwortmöglichkeiten aus. Mindestens eine Antwortmöglichkeit ist richtig.*

- [x] Globaler Rumpfkrafttest
- [x] 12 Minutenlauf
- [ ] Gewichtheben
- [ ] 100m-Sprint

### Challenge 03-02 [SC]
Welche Bewertung könnte für den Sportunterricht in Ihrem Zeugnis stehen?

Tipp: Für die Beantwortung dieser Frage könnte ein Dokument im Informationsteil dieser Station hilfreich sein.

- [ ] 4.5
- [x] üE
- [ ] Leistungssportler/-in
- [ ] Kraftprotz

### Challenge 03-03 [MC]
Welche Sportanlagen sind am GIBZ vorhanden?

*Wählen Sie eine oder mehrere Antwortmöglichkeiten aus. Mindestens eine Antwortmöglichkeit ist richtig.*

- [ ] Beachvolleyballfeld
- [x] Kletterwand
- [x] Kraftraum
- [ ] Squash-Halle

### Challenge 03-04 [MC]
Welche Sportanlagen sind am GIBZ vorhanden?

*Wählen Sie eine oder mehrere Antwortmöglichkeiten aus. Mindestens eine Antwortmöglichkeit ist richtig.*

- [x] Spinning Bikes
- [ ] Boxring
- [x] Tischtennis-Tische
- [ ] Hallenbad