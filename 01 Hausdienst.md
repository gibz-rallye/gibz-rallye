# 01 - Hausdienst

## Standort

- UUID: `221a4c7a-4ce6-4bea-a3ed-957c10e15780`
- Major: `101`

## Instruktion

Gehen Sie zum Büro des Hausdienstes.

Das Büro hat die Zimmernummer `1.002` und befindet sich im Erdgeschoss des Trakts 1.

## Information

Die Mitarbeitenden des Hausdienstes sind verantwortlich für die Gebäude-Infrastruktur, Ordnung und Sauberkeit am GIBZ. Zudem unterhält der Hausdienst ein **Fundbüro**, bei welchem Fundgegenstände abgegeben und abgeholt werden können.

Weitere Informationen zu den Aufgaben und Dienstleistungen des Hausdienstes finden Sie im Video.

## Anhänge

- Video: [https://storage.googleapis.com/ch_gibz_app_video/2023/01_Hausdienst.mp4](https://storage.googleapis.com/ch_gibz_app_video/2023/01_Hausdienst.mp4)

## Challenges

### Challenge 01-01 [SC]
Wie viele Fundgegenstände werden durchschnittlich **jede Woche** beim Hausdienst abgegeben?

- [ ] Etwa 1 Gegenstand
- [x] Etwa 5 Gegenstände
- [ ] Etwa 10 Gegenstände
- [ ] Etwa 20 Gegenstände

### Challenge 01-02 [TE]
Berechnen Sie die **einstellige Quersumme** der Telefonnummer (ohne Ländervorwahl) des Hausdienstes.

- [x] 4
- [x] vier

### Challenge 01-03 [MC]
Was gehört zu den Aufgaben des Hausdienstes?

*Wählen Sie eine oder mehrere Antwortmöglichkeiten aus. Mindestens eine Antwortmöglichkeit ist richtig.*

- [x] Reinigung der Räume
- [ ] Betrieb des WLAN
- [x] Fundgegenstände sammeln
- [x] Unterhalt der Gebäude und Infrastruktur

### Challenge 01-04 [MC]
Was gehört zu den Aufgaben des Hausdienstes?

*Wählen Sie eine oder mehrere Antwortmöglichkeiten aus. Mindestens eine Antwortmöglichkeit ist richtig.*

- [x] Abgabe des Liftschlüssels
- [ ] Stundenpläne erstellen
- [x] Fundgegenstände sammeln
- [x] Gebäude und Infrastruktur unterhalten