# 09 - Mikrowellen

## Standort

- UUID: `221a4c7a-4ce6-4bea-a3ed-957c10e15780`
- Major: `110`

## Instruktion

Gehen Sie zum Aufenthaltsraum `1.211` vor der Mediathek.

In diesem Raum finden Sie Mikrowellen, welche Sie zum Wärmen selbst mitgebrachter Speisen nutzen können. Zusätzlich finden Sie einen Snackautomaten.

## Information

Die Mikrowellen in diesem Aufenthaltsraum können Sie zum Wärmen selbst mitgebrachter Speisen verwenden.

Die Tische in diesem Aufenthaltsraum können Sie danach zum Essen der aufgewärmten Speisen oder ausserhalb der Mittagspause auch zum Lernen und für Gruppenarbeiten nutzen.

## Anhänge

*keine*

## Challenges

### Challenge 10-01 [SC]
Wie lange sollen **200 Gramm Reis** bei 750W gemäss der aufgehängten Kurzanleitung erhitzt werden?

- [ ] 1 Minute
- [x] 2 Minuten
- [ ] 3 Minuten
- [ ] 3.5 Minuten

### Challenge 10-02 [SC]
Wie lange soll ein **300 Gramm schweres Tellergericht** bei 750W gemäss der aufgehängten Kurzanleitung erhitzt werden?

- [ ] 1 Minute
- [ ] 2 Minuten
- [x] 3 Minuten
- [ ] 3.5 Minuten

### Challenge 10-03 [MC]
Wo am GIBZ stehen den Lernenden Mikrowellen zur Verfügung?

- [x] Im Aufenthaltsraum 1.211
- [ ] Im Restaurant Treff
- [ ] Beim Sekretariat
- [ ] Bei den Sporthallen