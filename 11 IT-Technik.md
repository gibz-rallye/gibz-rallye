# 11 - IT-Technik

## Standort

- UUID: `221a4c7a-4ce6-4bea-a3ed-957c10e15780`
- Major: `112`

## Instruktion

Gehen Sie zum Büro der IT-Technik.

Das Büro der IT-Technik befindet sich im Zimmer `1.102` im Trakt 1 auf dem 1. Stockwerk.

## Information

Die Mitarbeitenden der IT-Technik sorgen für den Betrieb und Unterhalt der IT-Infrastruktur am GIBZ. Dazu gehört beispielsweise die Verwaltung der *Microsoft 365* Lizenzen oder der Betrieb des WLAN am GIBZ.

Im Video erfahren Sie mehr über die Aufgaben und Dienstleistungen der IT-Technik.

## Anhänge

- Video: [https://storage.googleapis.com/ch_gibz_app_video/2023/11_IT-Technik.mp4](https://storage.googleapis.com/ch_gibz_app_video/2023/11_IT-Technik.mp4)

## Challenges

### Challenge 12-01 [SC]
Auf welcher Webseite können Sie Ihr Passwort ändern oder zurücksetzen?

- [ ] passwort.gibz.ch
- [ ] gibz.zg.ch
- [x] portal.gibz.ch
- [ ] eingang.gibz.ch

### Challenge 11-02 [MC]
Welches sind Aufgaben der IT-Technik?

*Wählen Sie eine oder mehrere Antwortmöglichkeiten aus. Mindestens eine Antwortmöglichkeit ist richtig.*

- [ ] Verkauf von Laptops
- [x] Herausgabe von Leihgeräten
- [x] Betrieb des WLAN
- [x] Betrieb und Support der audiovisuellen Hilfsmittel

### Challenge 11-03 [SC]
Darf das Internet am GIBZ auch für private Angelegenheiten genutzt werden?

- [ ] JA, uneingeschränkt
- [x] JA, aber nicht übertreiben
- [ ] NEIN, dies ist verboten

### Challenge 11-04 [SC]
Was sollen Lernende tun, wenn es ein Problem mit dem WLAN gibt?

- [ ] Das Problem dem Hausdienst melden
- [ ] Das Problem dem Sekretariat melden
- [x] Das Problem der IT-Technik melden
- [ ] Den eigenen Hotspot verwenden