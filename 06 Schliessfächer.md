# 06 - Schliessfächer

## Standort

- UUID: `221a4c7a-4ce6-4bea-a3ed-957c10e15780`
- Major: `107`

## Instruktion

An verschiedenen Standorten auf dem ganzen Areal des GIBZ sind Schliessfächer für das Deponieren persönlicher Gegenstände verfügbar.

Gehen Sie zur Schliessfachanlage neben dem Vorbereitungszimmer `3.108`.

## Information

Die Schliessfächer am GIBZ können mit einer App auf Ihrem Smartphone geschlossen und wieder geöffnet werden.

Im Video wird die Benutzung der Schliessfächer erklärt.

## Anhänge

- Video: [https://storage.googleapis.com/ch_gibz_app_video/2023/06_Schliessfaecher.mp4](https://storage.googleapis.com/ch_gibz_app_video/2023/06_Schliessfaecher.mp4)

## Challenges

### Challenge 07-01 [SC]
Wie lange kann ein Schliessfach *am Stück* maximal belegt werden?

- [x] 1 Tag
- [ ] 2 Tage
- [ ] 1 Woche
- [ ] 1 Semester

### Challenge 07-02 [TE]
Welche App müssen Sie auf Ihrem Smartphone installieren, um ein Schliessfach nutzen zu können?

- [x] Syphere Lockerbox
- [x] Syphere
- [x] Syphere Locker Box

### Challenge 07-03 [SC]
Können von einer Person mehrere Schliessfächer gleichzeitig belegt werden?

- [ ] ja
- [x] nein
- [ ] nur von Lehrpersonen