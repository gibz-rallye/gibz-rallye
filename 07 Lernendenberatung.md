# 07 - Lernendenberatung

## Standort

- UUID: `221a4c7a-4ce6-4bea-a3ed-957c10e15780`
- Major: `108`

## Instruktion

Gehen Sie zum Büro der Lernendenberatung.

Im Zimmer `4.201` werden Sie von **Stefan Rickli** und **Anita Vonchristen**, den beiden Lernendenberatenden des GIBZ, persönlich begrüsst.

## Information

Betreten Sie gemeinsam das Büro der Lernendenberatung. An einer der Wände finden Sie einen QR-Code für eine Umfrage.

**Jede Person** Ihres Teams scannt diesen Code mit dem Smartphone und füllt die Umfrage aus. Geben Sie anschliessend den Code ein, welchen Sie auf der letzten Seite der Umfrage erhalten.

## Anhänge

- Link: [Webseite der Lernendenberatung](https://www.zg.ch/behoerden/volkswirtschaftsdirektion/gibz/ueberuns/ansprechpersonen/lernendenberater)

## Challenges

### Challenge 08-01 [TE]
Geben Sie im untenstehenden Textfeld den Code ein, welcher nach Abschluss der Umfrage angezeigt wurde.

- [x] 5GH9+JX5