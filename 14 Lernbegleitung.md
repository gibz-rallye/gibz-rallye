# 14 - Lernbegleitung

## Standort

- UUID: `221a4c7a-4ce6-4bea-a3ed-957c10e15780`
- Major: `115`

## Instruktion

Gehen Sie zum Büro der Fachstelle *Lernbegleitung*.

Das Büro der Fachstelle Lernbegleitung befindet sich im Zimmer `2.101` im Trakt 2 auf dem 1. Stockwerk.

## Information

Die Fachstelle Lernbegleitung ist organisatorisch in vier verschiedene Bereiche gegliedert. Sie unterstützt die Lernenden und Studierenden am GIBZ in vielerlei Hinsichten.

Erfahren Sie mehr zu den Angeboten der Fachstelle Lernbegleitung im Video.

## Anhänge

- Video: [https://storage.googleapis.com/ch_gibz_app_video/2023/14_Lernbegleitung.mp4](https://storage.googleapis.com/ch_gibz_app_video/2023/14_Lernbegleitung.mp4)

## Challenges

### Challenge 15-01 [MC]
Das [Kursangebot](https://intern-gibz.lqc.ch/kursangebot) am GIBZ ist vielfältig. Wofür wird ein Kurs angeboten?

*Wählen Sie eine oder mehrere Antwortmöglichkeiten aus. Mindestens eine Antwortmöglichkeit ist richtig.*

- [x] 3D-Drucken
- [x] Filetieren von Fischen
- [ ] Handeln mit Bitcoins
- [x] Computer-Grundlagen

### Challenge 15-02 [MC]
Das [Kursangebot](https://intern-gibz.lqc.ch/kursangebot) am GIBZ ist vielfältig. Wofür wird ein Kurs angeboten?

*Wählen Sie eine oder mehrere Antwortmöglichkeiten aus. Mindestens eine Antwortmöglichkeit ist richtig.*

- [x] Steuererklärung ausfüllen
- [x] Soziale Kompetenzen
- [x] Grammatik (Deutsch)
- [ ] Trachten nähen

### Challenge 15-03 [MC]
Das [Kursangebot](https://intern-gibz.lqc.ch/kursangebot) am GIBZ ist vielfältig. Wofür wird ein Kurs angeboten?

*Wählen Sie eine oder mehrere Antwortmöglichkeiten aus. Mindestens eine Antwortmöglichkeit ist richtig.*

- [ ] Yoga
- [x] Deutsch als Zweitsprache (DaZ)
- [x] Spinning
- [x] Umgang mit Prüfungsangst