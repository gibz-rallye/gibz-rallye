# 08 - Mediathek

## Standort

- UUID: `221a4c7a-4ce6-4bea-a3ed-957c10e15780`
- Major: `109`

## Instruktion

Gehen Sie in den Aufenthaltsraum der Mediathek.

Die Mediathek befindet sich im obersten Stockwerk des Trakt 1, gleich über dem Restaurant *Treff*. Der Aufenthaltsraum der Mediathek ist jener Raum mit den gemütlichen Sofas.

## Information

Die Mediathek bietet ein umfangreiches Angebot für alle Lernenden und Studierenden am GIBZ. Dieses reicht von Büchern, Gesellschaftsspielen über Powerbanks bis hin zu einem vielfältigen Angebot verschiedener Online-Medien.

Schauen Sie sich das Video an, um mehr über die Mediathek und deren Angebote zu erfahren.

## Anhänge

- Video: [https://storage.googleapis.com/ch_gibz_app_video/2023/08_Mediathek.mp4](https://storage.googleapis.com/ch_gibz_app_video/2023/08_Mediathek.mp4)

## Challenges

### Challenge 09-01 [SC]
Wann ist die Mediathek während dem Schulbetrieb geöffnet?

- [ ] 07:00 - 14:00 Uhr
- [ ] 07:50 - 16:20 Uhr
- [ ] 08:00 - 16:30 Uhr
- [x] 08:30 - 17:00 Uhr

### Challenge 09-02 [MC]
Was kann in der Mediathek ausgeliehen werden?

*Wählen Sie eine oder mehrere Antwortmöglichkeiten aus. Mindestens eine Antwortmöglichkeit ist richtig.*

- [x] Spiele
- [x] Powerbanks
- [x] Taschenrechner
- [ ] Tageszeitungen
- [x] E-Books 

### Challenge 09-03 [MC]
Welche Dienstleistungen werden in der Mediathek angeboten?

*Wählen Sie eine oder mehrere Antwortmöglichkeiten aus. Mindestens eine Antwortmöglichkeit ist richtig.*

- [x] Unterstützung beim Recherchieren
- [ ] Kopier-Service für schulische Arbeiten
- [x] Zugang zu digitalen Medien
- [x] Einzel- und Gruppenarbeitsplätze