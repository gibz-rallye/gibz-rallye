# 10 - Restaurant Treff

## Standort

- UUID: `221a4c7a-4ce6-4bea-a3ed-957c10e15780`
- Major: `111`

## Instruktion

Gehen Sie zum Restaurant *Treff*.

Das Restaurant *Treff* befindet sich im Trakt 1 auf dem 1. Stockwerk.

## Information

Das Restaurant *Treff* verpflegt die Lernenden, Studierenden und Mitarbeitenden des GIBZ. Auch externe Gäste dürfen sich im Restaurant *Treff* verpflegen.

Neben verschiedenen Mittagsmenüs werden auch unterschiedliche Snacks, Süssigkeiten, Früchte und Getränke angeboten.

Schauen Sie sich das Video an, um noch mehr Informationen über das Restaurant *Treff* zu erfahren.

## Anhänge

- Video: [https://storage.googleapis.com/ch_gibz_app_video/2023/10_Restaurant_Treff.mp4](https://storage.googleapis.com/ch_gibz_app_video/2023/10_Restaurant_Treff.mp4)

## Challenges

### Challenge 11-01 [SC]
Wie viele Personen werden im Restaurant *Treff* täglich verpflegt?

- [ ] Ungefähr 100 Personen
- [x] Ungefähr 200 Personen
- [ ] Ungefähr 300 Personen
- [ ] Ungefähr 500 Personen

### Challenge 11-02 [SC]
Was kennzeichnet das **orange Symbol** des Menüleitsystems?

- [x] Vegane Speisen
- [ ] Vegetarische Speisen
- [ ] Genuss in Balance
- [ ] Glutenfrei

### Challenge 11-03 [SC]
Was kennzeichnet das **gelbe Symbol** des Menüleitsystems?

- [ ] Vegane Speisen
- [ ] Vegetarische Speisen
- [ ] Genuss in Balance
- [x] Glutenfrei

### Challenge 11-04 [MC]
Was ist im Restaurant *Treff* möglich?

*Wählen Sie eine oder mehrere Antwortmöglichkeiten aus. Mindestens eine Antwortmöglichkeit ist richtig.*

- [x] Speisen zum Mitnehmen kaufen
- [ ] Mitgebrachte Speisen erwärmen
- [x] Besteck für mitgebrachte Speisen kaufen
- [ ] Mitgebrachte Speisen verkaufen

### Challenge 11-05 [MC]
Wie kann der Menüplan des Restaurant *Treff* eingesehen werden?

*Wählen Sie eine oder mehrere Antwortmöglichkeiten aus. Mindestens eine Antwortmöglichkeit ist richtig.*

- [x] im Internet
- [x] in der GIBZ App
- [x] in einem Newsletter
- [x] am Aushang beim Restaurant Treff