# 13 - Infothek

## Standort

- UUID: `221a4c7a-4ce6-4bea-a3ed-957c10e15780`
- Major: `114`

## Instruktion

Gehen Sie zur Infothek.

Die Infothek befindet sich im Trakt 1 auf dem 1. Stockwerk direkt vor dem Eingang des Restaurants *Treff* und ist mit einem blauen `i` markiert.

## Information

In diesem Bereich finden Sie viele verschiedene Informationen.

Auf den **Info-Screens** über Ihren Köpfen werden aktuelle Informationen angezeigt. Zudem werden Sie auf diesen Bildschirmen über bevorstehende Anlässe informiert.

An der **Plakatsäule** beim Durchgang zum Trakt 3 sind unterschiedliche Plakate zu Veranstaltungen angeschlagen. Bestimmt ist auch für *Sie* etwas Interessantes dabei.

Bei der Infothek sind verschiedene Flyer rund um ergänzende Angebote des GIBZ aufgelegt.

An der Wand neben dem Aufenthaltszimmer für Mitarbeitende ist zudem einer von insgesamt zwei **Defibrillatoren** am GIBZ befestigt. Das andere lebensrettende Gerät befindet sich beim Haupteingang des Trakt 1.

## Anhänge

*keine*

## Challenges

### Challenge 14-01 [SC]
Wofür steht das Symbol der Willkommensnachricht auf dem Info-Screen?

- [ ] Erfolg
- [ ] Leistung
- [ ] Kraft
- [x] Glück

### Challenge 14-02 [SC]
In einem roten Kasten neben dem Zimmer *Aufenthalt Mitarbeitende* befindet sich ein *Defibrillator*. Wofür kann dieses Gerät genutzt werden?

- [ ] COVID-19 Test
- [x] Natürlichen Herzrhythmus wiederherstellen
- [ ] Blutdruck messen
- [ ] Beatmung für Asthmatiker:innen