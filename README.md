# GIBZ Rallye

Auf diesen Seiten sind sämtliche Inhalte der GIBZ Rallye dokumentiert. Da diese Dokumentation auch alle *Challenges* (inkl. Antworten) enthält, soll sie vertraulich behandelt werden (d.h. insbesondere nicht an Lernende weitergegeben werden).

## Gliederung nach Stationen

Die Inhalte sind nach Stationen der GIBZ Rallye gegliedert. Für jede der insgesamt 16 Stationen existiert ein separater Abschnitt. Innerhalb eines solchen Abschnittes sind die Inhalte jeweils gleichartig aufgebaut.

### Standort

Unter dem Titel _Standort_ sind zwei technische Merkmale aufgeführt, welche inhaltlich keine Bedeutung haben. Die Werte von _UUID_ und _Major_ werden vom entsprechenden Beacon ausgesendet und identifizieren damit den Standort einer Station der GIBZ Rallye.

### Instruktion

Die *Instruktion* einer Station wird in der GIBZ App angezeigt, bevor die Lernenden den Standort der jeweiligen Station erreicht haben. Sobald die Lernenden am entsprechenden Standort eintreffen, wird in der App automatisch zum Bildschirm mit der *Information* gewechselt.

### Information

Die *Information* einer Station wird in der GIBZ App angezeigt, sobald die Lernenden am Standort der Station eintreffen. Zusammen mit dem Text der Information werden auch alle *Anhänge* (Video, Links) dargestellt.

### Challenges

In der GIBZ App können die Lernenden selbständig vom Bildschirm mit der *Information* zur *Challenge* wechseln. Jedem Team wird pro Station jeweils genau eine Challenge präsentiert. Sofern für eine Station mehrere Challenges erfasst sind, wird eine *zufällige* Challenge bestimmt und dargestellt.

Es gibt insgesamt 3 verschiedene Typen von *Challenges*:

- **Single-Choice [SC]**: Aus einem vorgegebenen Set von Antworten können die Lernenden **genau 1** richtige Antwort auswählen. Die Lernenden erhalten dafür entweder `0` Punkte (falsche Antwort) oder das Punktemaximum der jeweiligen Challenge (richtige Antwort).
- **Multiple-Chice [MC]**: Aus einem vorgegebenen Set von Antworten können die Lernenden **mindestens 1** richtige Antwort auswählen. Die Punktezahl wird ausgehend vom Punktemaximum berechnet.
- **Texteingabe [TE]**: Die Lernenden können ihre Antwort in ein Textfeld eingeben. Alle aufgeführten Antworten sind potentiell richtige Antworten. Die Lernenden erahlten für Ihre Antwort entweder `0` Punkte (falsche Antwort) oder das Punktemaximum der jeweiligen Challenge (richtige Antwort). Für die Bewertung der Antwort ist die Gross-/Kleinschreibung irrelevant.

Unabhängig vom Typ der Challenge wird die richtige Lösung für die Teilnehmenden der Rallye *nicht angezeigt*. Diese Entscheidung ist wohl aus rein pädagogischer Sicht umstritten. Es wurde dennoch auf die Anzeige der richtigen Lösung verzichtet, um das Austauschen der richtigen Lösungen zu erschweren.
