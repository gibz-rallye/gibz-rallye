# 03 - Recycling

## Standort

- UUID: `221a4c7a-4ce6-4bea-a3ed-957c10e15780`
- Major: `104`

## Instruktion

Verlassen Sie den Trakt 2 beim Ausgang Richtung _Mattenstrasse_.

Der nächste Stopp der GIBZ Rallye befindet sich bei der Recycling-Station dieses Ausgangs.

## Information

Am GIBZ sind wir bemüht, möglichst wenig Abfall zu produzieren. Anfallende Abfälle werden getrennt gesammelt und nach Möglichkeit verwertet.

Im Video erfahren Sie wissenswertes über das Recycling am GIBZ.

## Anhänge

- Video: [https://storage.googleapis.com/ch_gibz_app_video/2023/03_Recycling.mp4](https://storage.googleapis.com/ch_gibz_app_video/2023/03_Recycling.mp4)

## Challenges

### Challenge 04-01 [SC]
Wie viele PET-Flaschen werden am GIBZ jährlich gesammelt und rezykliert?

- [ ] Ungefähr 10'000 Flaschen
- [ ] Ungefähr 25'000 Flaschen
- [ ] Ungefähr 100'000 Flaschen
- [x] Ungefähr 200'000 Flaschen

### Challenge 04-02 [MC]
Welcher der folgenden Abfälle wird am GIBZ *nicht* separat gesammelt?

*Wählen Sie eine oder mehrere Antwortmöglichkeiten aus. Mindestens eine Antwortmöglichkeit ist richtig.*

- [ ] PET
- [ ] Papier
- [x] Alu
- [ ] Pizzakartons

### Challenge 04-03 [MC]
Wer hilft mit, die Abfälle am GIBZ korrekt zu trennen?

*Wählen Sie eine oder mehrere Antwortmöglichkeiten aus. Mindestens eine Antwortmöglichkeit ist richtig.*

- [x] Der Hausdienst
- [x] Die Lehrpersonen
- [x] Die Lernende
- [x] Die Schulleitung