# 05 - Parkgarage

## Standort

- UUID: `221a4c7a-4ce6-4bea-a3ed-957c10e15780`
- Major: `106`

## Instruktion

Gehen Sie zur Kasse in der Parkgarage.

Die Kasse der Parkgarage befindet sich im Untergeschoss des Trakts 3.

## Information

Die Anreisemöglichkeiten zum GIBZ sind vielfältig: zu Fuss, mit öffentlichen Verkehrsmitteln, mit dem Auto, Motorrad, Mofa oder dem Velo. Für die vier letztgenannten Verkehrsmittel existieren in der Tiefgarage des GIBZ Parkmöglichkeiten.

Erfahren Sie im Video alles Wissenswerte über das Parkieren von privaten Fahrzeugen am GIBZ.

*Falls Sie das Video wegen schlechtem Empfang nicht anschauen können: Verlassen Sie die Parkgarage und spielen Sie das Video im Eingangsbereich des Trakts 3 ab.*

## Anhänge

- Video: [https://storage.googleapis.com/ch_gibz_app_video/2023/05_Parkgarage.mp4](https://storage.googleapis.com/ch_gibz_app_video/2023/05_Parkgarage.mp4)

## Challenges

### Challenge 06-01 [MC]
Welche Wohnorte berechtigen zum Bezug einer Saldo-Karte für vergünstigtes Parkieren am GIBZ?

*Falls Sie die Antwort für diese Challenge nicht absenden können: Verlassen Sie die Parkgarage für besseren Empfang und versuchen Sie es danach erneut.*

*Wählen Sie eine oder mehrere Antwortmöglichkeiten aus. Mindestens eine Antwortmöglichkeit ist richtig.*

- [ ] Cham
- [ ] Stadt Zürich
- [x] Altdorf
- [x] Lenzburg

### Challenge 06-02 [TE]
Sie parkieren Ihr Auto für einen Schultag (9 Stunden) im Parkhaus des GIBZ. Wie hoch sind die Parkkosten *ohne* Saldo-Karte?

*Falls Sie die Antwort für diese Challenge nicht absenden können: Verlassen Sie die Parkgarage für besseren Empfang und versuchen Sie es danach erneut.*

- [x] 23
- [x] 23 Franken
- [x] 23.00
- [x] 23.-
- [x] 23 Fr.
- [x] 23 Fr
- [x] 23 Sfr.
- [x] 23 Sfr

### Challenge 06-03 [SC]
Zu welchen Zeiten ist die Parkgarage während der Schulzeit jeweils geöffnet?

*Falls Sie die Antwort für diese Challenge nicht absenden können: Verlassen Sie die Parkgarage für besseren Empfang und versuchen Sie es danach erneut.*

- [ ] 06:00 - 17:00 Uhr
- [x] 06:00 - 24:00 Uhr
- [ ] 07:00 - 19:00 Uhr
- [ ] 07:00 - 24:00 Uhr

### Challenge 06-04 [MC]
Womit kann die Parkgebühr bezahlt werden?

*Falls Sie die Antwort für diese Challenge nicht absenden können: Verlassen Sie die Parkgarage für besseren Empfang und versuchen Sie es danach erneut.*

*Wählen Sie eine oder mehrere Antwortmöglichkeiten aus. Mindestens eine Antwortmöglichkeit ist richtig.*

- [x] Bargeld (Franken)
- [x] Euro
- [x] Kreditkarte (Master, Visa)
- [ ] Bitcoin
- [x] Twint

### Challenge 06-05 [MC]
Wo wird die aktuelle Zahl der freien Parkplätze angezeigt?

*Falls Sie die Antwort für diese Challenge nicht absenden können: Verlassen Sie die Parkgarage für besseren Empfang und versuchen Sie es danach erneut.*

*Wählen Sie eine oder mehrere Antwortmöglichkeiten aus. Mindestens eine Antwortmöglichkeit ist richtig.*

- [ ] Bei der Einfahrt der Parkgarage
- [ ] Auf den Info-Screens im Schulhaus
- [ ] Auf der Webseite des GIBZ
- [x] In der GIBZ App