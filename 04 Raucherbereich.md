# 04 - Raucherbereich

## Standort

- UUID: `221a4c7a-4ce6-4bea-a3ed-957c10e15780`
- Major: `105`

## Instruktion

Gehen Sie zum gedeckten Raucherbereich zwischen Trakt 1 und Trakt 3.

## Information

Das Rauchen ist am GIBZ nur in den markierten Bereichen gestattet.

Schauen Sie sich das Video an, um mehr zu erfahren.

## Anhänge

- Video: [https://storage.googleapis.com/ch_gibz_app_video/2023/04_Raucherbereich.mp4](https://storage.googleapis.com/ch_gibz_app_video/2023/04_Raucherbereich.mp4)

## Challenges

### Challenge 05-01 [MC]
Was darf in den ausgewiesenen Bereichen am GIBZ geraucht werden?

*Wählen Sie eine oder mehrere Antwortmöglichkeiten aus. Mindestens eine Antwortmöglichkeit ist richtig.*

- [x] Tabak
- [ ] Marihuana
- [ ] CBD-Hanf
- [x] E-Zigaretten

### Challenge 05-02 [MC]
Wo darf am GIBZ geraucht werden?

*Wählen Sie eine oder mehrere Antwortmöglichkeiten aus. Mindestens eine Antwortmöglichkeit ist richtig.*

- [ ] Auf den Terrassen
- [ ] Im Schulzimmer bei geöffnetem Fenster
- [ ] In der Tiefgarage
- [x] Draussen, im ausgewiesenen Raucherbereich

### Challenge 05-03 [SC]
Wie viele Zigarettenstummel müssen durch den Hausdienst **jeden Tag** vom Boden aufgesammelt werden, weil diese nicht korrekt entsorgt wurden?

- [ ] Weniger als 10
- [ ] Knapp 20
- [ ] Zwischen 40 und 60
- [x] Mehr als 100